#!/usr/bin/env python
#coding=utf-8

#case 1
#name = raw_input('please input your name')
#print 'hello,',name

#case 2
#print '''line1
#line2
#line3'''
 
#case 3
#temp = 42
#print "The temperature is " + str(temp)

#case 4
#根据给定的年月日以数字形式打印出日期
# months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

# #以1-31的数字作为结尾的列表
# endings = ['st', 'nd', 'rd'] + 17 * ['th'] + ['st', 'nd', 'rd'] + 7 * ['th'] + ['st']

# year = raw_input('Year:')
# month = raw_input('Month(1-12):')
# day = raw_input('Day(1-31):')

# month_num = int(month)
# day_num = int(day)

# month_name = months[month_num - 1]
# day_name = day + endings[day_num - 1]

# print month_name + ' ' + day_name + ',' + year


#case 5
# f = open('f.txt','w')
# for i in range(0, 10):
# 	f.write(str(i)+'\n')

# f.close()

#case 6
# url = raw_input('please input the url:')
# domain = url[11:-4]

# print "domain is :" + domain



#case 7
def my_abs(x):
	if not isinstance(x, (int, float)):
		raise TypeError('bad operand type')
	if x >= 0:
		return x
	else:
		return -x