#!/usr/bin/env python
# coding=utf-8

import os

n = raw_input('Please enter the number of files you want to create: ')

num = int(n)
print num

rootPathName = os.path.abspath('.')

# 创建N个.txt文件，且文件内容与文件名一致
for i in range(num):
    name = str(i + 1)
    newPathName = os.path.join(rootPathName, name)
    os.mkdir(newPathName)
    newFileName = os.path.join(newPathName, name + '.txt')
    with open(newFileName, 'w') as f:
        f.write(name+'\n')