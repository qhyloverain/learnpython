# Python快速入门文档
## 零、学习资源总结 
### 0.0 文档 
* 官方文档: 
[Python Documentation](http://docs.python.org)
* 第三方文档/Wiki类： 
[tutorialpoints](https://www.tutorialspoint.com/python/) 
 
### 0.1 工具 
---
#### 解释器/交互环境 
##### 解释器 
* CPython：官方版本 
* IPython： Cpython 
* PyPy 
* JPython 
 
##### 交互环境 
* Python Shell: 官方的Shell环境 
* IPython: 第三方交互环境，更好的补全和运行指令 
	* [使用iPython进行交互式作业](https://itacey.gitbooks.io/learning_ipython/content/%E7%AC%AC%E4%BA%8C%E7%AB%A0.html) 
	* [iPython介绍](https://www.ctolib.com/topics-79339.html)

---
#### IDE 
* PyCharm: JetBrains出品，必属精品，写Python项目必备 
* Visual Studio: 在最新的VS版本都加入了Python的支持 
其他没有用过 

--- 
#### 编辑器 
与IDE相比，编辑器比较轻便，适合简易的脚本编写。 
以下几个常用编辑器都对Python的支持，部分加上插件后效率更高，可以和IDE媲美，在这里不再对比谁优谁劣 

* Sublime Text 
* Visual Studio Code 
* Atom 
* Vim/Emacs 

---
#### 包管理/环境 
* Pip: 默认Python包管理工具，必备 
* 包管理环境工具: 
	* Virtualenv: 一个Python的包管理模块，方便各项目的独立管理 
	* VirtualenvWrapper: Virtualenv的升级版，方便操作 
* 独立的环境：完整独立的一个环境，已配备了很多数据计算的lib, 常用于科学计算和分析 
	* [Anaconda](https://www.continuum.io/downloads) 
	* [Canopy](https://www.enthought.com/products/canopy/) 
	
---
 
### 0.2 教程 
* [The python guru](http://thepythonguru.com/)
* [The hitchhiker's Guide to Python](http://docs.python-guide.org/en/latest/writing/documentation/)
* [Dive Into Python](http://www.diveintopython.net/toc/index.html)
* [Google Python Class](https://www.youtube.com/watch?v=tKTZoB2Vjuk): 需翻墙
* [Runoob.com菜鸟教程](http://www.runoob.com/python/python-tutorial.html) 
* [廖雪峰教程](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000) 

### 0.3 书籍 
略，不推荐 

### 0.4 其他资源 
* 第三方库搜索
 	* [PyPI](https://pypi.python.org/pypi)
	* Awsome Python:
* Github:  
	* [vinta/awesome-python](https://github.com/vinta/awesome-python) 
	* [trananhkma/fucking-awesome-python](https://github.com/trananhkma/fucking-awesome-python) 

### 0.5 代码规范 
* [PEP8](http://pep8.org/) 
 
 
 
## 一、入门学习 
### Python简介 
* 历史、发展及现状
	* “龟叔”Guido von Rossum发明于1989年
	* 通过社区完善功能与标准，Python开发者将不同领域的优点带给Python 
	* 2.0开始从maillist转为完全开源的开发模式，获得更加高速的发展
	* 如今，Python的框架已经确立。
		* Python语言以对象为核心组织代码(Everything is object)
		* 支持多种编程范式(multi-paradigm)
		* 采用动态类型(dynamic typing)
		* 自动进行内存回收(garbage collection)
		* Python支持解释运行(interpret)，并能调用C库进行拓展
		* Python有强大的标准库 (battery included)
		* 有丰富的第三方包，如Django, web.py, wxpython, numpy, matplotlib,PIL，将Python升级成了物种丰富的热带雨林。
	* TIOBE语言排行第八，Google的第三大开发语言
* 特性/优点 
	* 提供完善的基础代码库，覆盖范围广，方便直接使用现成资源
	* 有大量第三方库，且个人的代码经过封装也可作为第三方库调用	 
	
* 缺点 
	* 运行速度慢；由于是解释型语言，代码执行时逐行翻译成机器码，翻译过程十分耗时
	* 代码不能加密；发布Python程序，就是公开源代码
 
### 测试为什么选择Python？ 
#### 自动化需要脚本语言 
对于大多数从测试岗位而言，测试当中有许许多多重复而繁琐的操作，如测试内容本身：准备大量高门槛测试数据，基础核心功能回归，性能测试，稳定性测试等；还有测试管理方面：测试用例的管理等等。 这些操作如果依靠人工去完成，可谓是费时费力，而且稍不留神就可能有出错的可能，因此通过一套标准的自动化测试流程来完成，是测试的最好选择，而实现这一套流程，在大多数的时候，依靠的就是脚本语言。

脚本语言有多种多样，Shell, Ruby, Perl, Python等，这些脚本语言各具特点，如系统本身提供的命令行，即Shell语言（Windows上是Bash批处理），能通过简单的逻辑指令、运算操作及管道操作来完成稍微复杂的系统任务。举个例子，最初的垃圾清理软件，就是通过搜索系统当中以".tmp"等后缀的文件来进行删除来完成清楚系统垃圾。
#### Python的特性
那么Python在当中，对于测试来说，优势在哪？ 通过一段时间的学习以及了解，简单总结如下：
##### 胶水性
胶水性（glue），是Python的最大特征，因为除了用于测试脚本外，Python的社区当中拥有众多的库支持，如科学计算的Numpy, 机器学习的ScikitLearn，以及众多的后端开发框架，就连OpenCV也将Python作为其为数不多的上层接口语言等等，因而它的功能是非常丰富的，对于需要快速开发出软件的雏形或者验证算法的可行性，Python可谓是哆啦A梦的百宝袋。
在测试这块，Python有着自带的单元测试模块unittest和记录模块logging，简化了测试开发的时间。同时众多测试框架，如Appium和selenium支持Python，可谓对Python是极其友好。
##### 简洁、优雅 
Python在定位上是简洁而优雅的。对于测试脚本而言，脚本的通用性是比较弱的，每次完成特定任务就需要有一个新的脚本，亦或者脚本需要随着进度开发而不断调整，那么除了需要对于模块的封装，简洁而易读的语句将是测试开发的福音。

除此之外，每每需要对脚本进行修改，如果任务复杂，在众多语句当中寻找需要修改的部分必定是苦不堪言的。更进一步，如果前人的代码还没有注释，那么估计想要调整对还真的不是那么容易。

尤其是近年来，"Code is comment"的口号不断，易读的代码就是最好的注释，而Python的语句通常能够完成其他语言多倍的任务，这样的优势使它在所有高级语言里面显得更加高级。
##### 易上手和学习
在许许多多编程语言中，Python在国外早已成为零基础的菜鸟的入门语言，可见其易入手的程度。

首先它作为一门解释型语言，使得它拥有动态的数据类型，这对许多刚开始接触编程的人来说是非常友好的，不需要考虑数据类型后面的内存管理，更不用担心去理解C/C++语言当中指针的使用。其次，丰富的数据结构带来的优势使得它在处理数据和文本能力优于C/C++或者是Java等语言，如list和dict，极大地简化了对数据的存储和使用。最后，它的学习曲线在众多语言当中应该是比较平缓的，每每接触新的语法特性，都会感觉是那么自然，让人觉得就是自然语言一般。
 
### 如何快速入门Python 
在我读大学的时候，首先接触的编程语言是C和C++，当时学校会把这门语言的每个部分进行拆解，如分成输入输出部分，数据类型，if-else和for循环等等，而在我接触另一门语言Java或者是JS时，很欣慰地发现它和C++竟是如此相似，几乎就可以上手即用的感觉，给我留下深刻印象。

而如今，在面对不同任务时，往往需要快速学习一门语言。而这应该是现在每一个程序员的必备能力，那么回到我们的话题，究竟如何能够快速入门Python呢？

最近，有个朋友给我转发了一篇文章，[《如何掌握所有的程序语言》](http://www.yinwang.org/blog-cn/2017/07/06/master-pl)，是之前微软大神王垠写的。我读了之后，恍然大悟，快速入门一门语言的关键，就是学习理解这门语言的语言特性。

在文章中，解释了什么是语言特性，在这举一些例子，如变量定义、算术运算、for 循环语句，while 循环语句、函数定义，函数调用、递归、面向对象、垃圾回收、指针算术、goto 语句等等，它几乎就是这个语言的一个配置，而当中有一部分是基本配置，这是我们能够用这门语言的关键，而能够熟练运用这门语言，或者说是擅长使用，则需要我们花时间去学习这门语言当中的更加高级复杂的配置。

举个例子，C语言在过去一直被认作是面向过程的语言，它的执行就是按着语句的编排顺序来完成。而它的继任者C++则是多了面向对象的特性， 而近年来，C++则是不断吸收其他语言的特性，如加入了Lambda函数、实质就是像Python一样的函数式编程，以及支持auto类型，也是学习如Python一类语言的动态类型，只不过它们在实现的方式不同。

所以基本来看，要快速入门Python，我们只需要根据我们的需要，去掌握基本的语言特性，如数据类型定义、if-else语句、for,while循环和简单的输入输出即可。而如果需要完成测试任务，需要接触到更多的高级配置，如os模块，文件读写，以及更多的单元测试和记录Log的模块。

最后，要灵活运用还是少不了代码练手，通过写几个小项目或者完成某些特定的任务，能够让人快速熟悉Python里面的特性。
 
 
